/*
  Copyright (C) 2012 Daniel Sudmann

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.

  Daniel Sudmann suddani@googlemail.com
*/

#include <core_net/CConnectionManager.h>
#include <core_net/CConnection.h>
#include <core_net/IConnectionMessageReceiver.h>
#include "CNetDataStream.h"

#include <core/ILog.h>
#include <core/IDataStream.h>

bool CConnectionManager::initialized = false;

void CConnectionManager::initialize()
{
    if (CConnectionManager::initialized)
        return;
    if (enet_initialize () != 0)
    {
        nlerror<<"An error occurred while initializing ENet"<<nlendl;
        return;
    }

    nlog<<"Initialized ENet"<<nlendl;
    CConnectionManager::initialized = true;
}


CConnectionManager::CConnectionManager()
{
    CConnectionManager::initialize();
    host = NULL;
}

CConnectionManager::~CConnectionManager()
{
    if (host)
        enet_host_destroy(host);
    host = NULL;
}

bool CConnectionManager::init(const short& port, const int& max_connections)
{
    ENetAddress address;
    /* Bind the server to the default localhost.     */
    /* A specific host address can be specified by   */
    /* enet_address_set_host (& address, "x.x.x.x"); */
    address.host = ENET_HOST_ANY;
    /* Bind the server to port. */
    address.port = port;
    host = enet_host_create ((port != 0 ? &address : 0) /* the address to bind the server host to */,
                             max_connections        /* allow up to 32 clients and/or outgoing connections */,
                             CC_COUNT              /* allow up to 2 channels to be used, 0 and 1 */,
                             0                     /* assume any amount of incoming bandwidth */,
                             0                     /* assume any amount of outgoing bandwidth */);
    if (host == NULL)
    {
        nlerror<<"An error occurred while trying to create an ENet server host"<<nlendl;
        exit (EXIT_FAILURE);
        return false;
    }

    for (unsigned int i=0; i<host->peerCount; i++)
        host->peers[i].data = NULL;

    //add compression
    enet_host_compress_with_range_coder(host);

    return true;
}

bool CConnectionManager::connect(const char* address_, const short& port)
{
    ENetAddress address;

    ENetPeer *peer;
    /* Connect to some.server.net:1234. */
    enet_address_set_host (& address, address_);
    address.port = port;
    /* Initiate the connection, allocating the two channels 0 and 1. */
    peer = enet_host_connect (host, & address, CC_COUNT, 33133);

    return peer != NULL;
}



void CConnectionManager::send(AppFrame::IDataStream* stream, const CONNECTION_CHANNEL& channel)
{
    enet_uint32 packet_flags = 0;
    if (channel == CC_DATA_RELIABLE || channel == CC_ACTOR_EVENT_RELIABLE || channel == CC_EVENT_RELIABLE)
        packet_flags = ENET_PACKET_FLAG_RELIABLE;

    ENetPacket * packet = enet_packet_create (stream->getData(), stream->getSize(), packet_flags);
    enet_host_broadcast(host, channel, packet);
    enet_host_flush(host);
}

void CConnectionManager::send(const ConnectionID& connection, AppFrame::IDataStream* stream, const CONNECTION_CHANNEL& channel)
{
    ENetPeer* peer = getConnectionById(connection)->getPeer();
    if (!peer)
        return;

    enet_uint32 packet_flags = 0;
    if (channel == CC_DATA_RELIABLE || channel == CC_ACTOR_EVENT_RELIABLE || channel == CC_EVENT_RELIABLE)
        packet_flags = ENET_PACKET_FLAG_RELIABLE;

    ENetPacket * packet = enet_packet_create (stream->getData(), stream->getSize(), packet_flags);
    enet_peer_send(peer, channel, packet);
    enet_host_flush(host);
}

void CConnectionManager::update()
{
    ENetEvent event;

    /* Wait up to 1 millisecond for an event. */
    while (enet_host_service (host, & event, 1) > 0)
    {

        switch (event.type)
        {
        case ENET_EVENT_TYPE_NONE:
            nlerror<<"WTF how did you get here"<<nlendl;
            break;
        case ENET_EVENT_TYPE_CONNECT:
        {
            /* Store any relevant client information here. */
            //event.peer -> data = "Client information";
            CConnection* connection = new CConnection(event.peer);

            connections.push_back(connection);


            nlog<<"A new client connected as "<<connection->getId()<<" Data["<<event.data<<"]"<<nlendl;
            if (messageReceiver)
                messageReceiver->onConnect(connection->getId());
        }

        break;

        case ENET_EVENT_TYPE_RECEIVE:
        {
            CConnection* connection = getConnectionByPeer(event.peer);
            if (connection)
            {
                CNetDataStream stream((char*)event.packet -> data, event.packet -> dataLength);

                //nlog<<"A packet of length "<<event.packet -> dataLength<<" containing "<<data.c_str()<<" was received from "<<event.peer -> data<<" on channel "<<event.channelID<<nlendl;

                if (messageReceiver)
                    messageReceiver->onMessage(connection->getId(), (CONNECTION_CHANNEL)event.channelID, &stream);
            }

            /* Clean up the packet now that we're done using it. */
            enet_packet_destroy (event.packet);
        }
        break;

        case ENET_EVENT_TYPE_DISCONNECT:
        {
            CConnection* connection = getConnectionByPeer(event.peer);
            if (connection)
            {

                if (messageReceiver)
                    messageReceiver->onDisconnect(connection->getId());

                nlog<<connection->getId()<<" disconected"<<nlendl;

                /* Reset the peer's client information. */
                for (int i=0; i<getConnectionCount(); i++)
                {
                    if (connection == connections[i])
                    {
                        connections.erase(connections.begin()+i);
                        break;
                    }
                }
            }
        }
        break;
        }
    }
}

void CConnectionManager::setMessageReceiver(IConnectionMessageReceiver* receiver)
{
    messageReceiver = receiver;
}

int CConnectionManager::getConnectionCount(void)
{
    return connections.size();
}

CConnection* CConnectionManager::getConnection(const int& id)
{
    if (id < 0 || id >= getConnectionCount())
        return NULL;
    return connections[id];
}

CConnection* CConnectionManager::getConnectionById(const ConnectionID& id)
{
    for (int i=0; i<getConnectionCount(); i++)
    {
        if (connections[i]->getId() == id)
            return connections[i];
    }
    return NULL;
}

CConnection* CConnectionManager::getConnectionByPeer(ENetPeer* peer)
{
    for (int i=0; i<getConnectionCount(); i++)
    {
        if ((*connections[i]) == peer)
            return connections[i];
    }
    return NULL;
}

std::vector<AppFrame::SharedPointer<CConnection> > CConnectionManager::getConnections(void)
{
    return connections;
}



