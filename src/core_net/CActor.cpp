/*
  Copyright (C) 2012 Daniel Sudmann

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.

  Daniel Sudmann suddani@googlemail.com
*/

#include <core_net/CActor.h>
#include <core_net/CActorReplicationState.h>
#include <core/IDataStream.h>

CActor::CActor(const int& class_id_, bool tmp)
{
    init(class_id_, tmp);
    nlog<<"Create CActor"<<nlendl;
}

CActor::CActor(const int& class_id_, const int& actor_id, bool tmp)
{
    init(class_id_, tmp);
    id = actor_id;
    nlog<<"Create CActor: "<<id<<nlendl;
}

CActor::~CActor(void)
{
    nlog<<"Destroy CActor["<<id<<"]"<<nlendl;
}

bool CActor::isTemp(void)
{
    return bNetTemporary;
}


void CActor::init(const int& class_id_, bool tmp)
{
    bAlwaysRelevant = false;
	bOnlyRelevantToOwner = false;

	bHidden = false;
	bBlockPlayers = false;

	//bStatic = false;
	//bNoDelete = false;

	bNetTemporary = tmp;
	bTearOff = false;

	class_id = class_id_;
}

void CActor::serialize(AppFrame::IDataStream* out, CActorReplicationState* replication_state)
{
    out->Write(id);
    out->Write(class_id);
    out->Write(bNetTemporary);
    //out->Write(bTearOff);
    //now send everything else

}

void CActor::deserialize(AppFrame::IDataStream* in, CActorReplicationState* replication_state)
{
}

void CActor::EventInit(const ConnectionID& id)
{

}

void CActor::EventRemoved(void)
{

}

int CActor::getId(void)
{
    return id;
}

void CActor::setId(int id_)
{
    id = id_;
}

int CActor::getClassId(void)
{
    return class_id;
}





