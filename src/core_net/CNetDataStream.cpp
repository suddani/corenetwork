/*
  Copyright (C) 2012 Daniel Sudmann

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.

  Daniel Sudmann suddani@googlemail.com
*/

#include "CNetDataStream.h"
#include <enet/enet.h>

unsigned int htonf(float f)
{
    unsigned int p;
    unsigned int sign;

    if (f < 0)
    {
        sign = 1;
        f = -f;
    }
    else
    {
        sign = 0;
    }

    p = ((((unsigned int)f)&0x7fff)<<16) | (sign<<31); // whole part and sign
    p |= (unsigned int)(((f - (int)f) * 65536.0f))&0xffff; // fraction

    return p;
}

float ntohf(unsigned int p)
{
    float f = ((p>>16)&0x7fff); // whole part
    f += (p&0xffff) / 65536.0f; // fraction

    if (((p>>31)&0x1) == 0x1)
    {
        f = -f;    // sign bit set
    }

    return f;
}

//ENET_NET_TO_HOST_16, ENET_HOST_TO_NET_16, ENET_NET_TO_HOST_32, and ENET_HOST_TO_NET_32

CNetDataStream::CNetDataStream(AppFrame::c8* data, AppFrame::u32 size, bool copy) : AppFrame::CDataStream(data, size, copy)
{

}

CNetDataStream::CNetDataStream(AppFrame::u32 reserve) : AppFrame::CDataStream(reserve)
{

}

CNetDataStream::CNetDataStream(const CDataStream& data) : AppFrame::CDataStream(data)
{

}


CNetDataStream::CNetDataStream() : AppFrame::CDataStream()
{
    //ctor
}

CNetDataStream::~CNetDataStream()
{
    //dtor
}

void CNetDataStream::Write(const AppFrame::s32& data)
{
    AppFrame::s32 _data = ENET_HOST_TO_NET_32(data);
    CDataStream::Write(_data);
}

void CNetDataStream::Write(const AppFrame::u32& data)
{
    AppFrame::u32 _data = ENET_HOST_TO_NET_32(data);
    CDataStream::Write(_data);
}

void CNetDataStream::Write(const AppFrame::s16& data)
{
    AppFrame::s16 _data = ENET_HOST_TO_NET_16(data);
    CDataStream::Write(_data);
}

void CNetDataStream::Write(const AppFrame::u16& data)
{
    AppFrame::u16 _data = ENET_HOST_TO_NET_16(data);
    CDataStream::Write(_data);
}

void CNetDataStream::Write(const AppFrame::f32& data)
{
    CDataStream::Write(htonf(data));
}

bool CNetDataStream::Read(AppFrame::s32& data)
{
    bool read = CDataStream::Read(data);
    data = ENET_NET_TO_HOST_32(data);
    return read;
}

bool CNetDataStream::Read(AppFrame::u32& data)
{
    bool read = CDataStream::Read(data);
    data = ENET_NET_TO_HOST_32(data);
    return read;
}

bool CNetDataStream::Read(AppFrame::s16& data)
{
    bool read = CDataStream::Read(data);
    data = ENET_NET_TO_HOST_16(data);
    return read;
}

bool CNetDataStream::Read(AppFrame::u16& data)
{
    bool read = CDataStream::Read(data);
    data = ENET_NET_TO_HOST_16(data);
    return read;
}

bool CNetDataStream::Read(AppFrame::f32& data)
{
    bool read = CDataStream::Read(data);
    data = ntohf(data);
    return read;
}

