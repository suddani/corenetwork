#ifndef CNETDATASTREAM_H
#define CNETDATASTREAM_H

#include <core/../../source/core/CDataStream.h>

class CNetDataStream : public AppFrame::CDataStream
{
    public:
        CNetDataStream(AppFrame::c8* data, AppFrame::u32 size, bool copy = true);
        CNetDataStream(AppFrame::u32 reserve);
        CNetDataStream(const CDataStream& data);
        CNetDataStream(void);
        virtual ~CNetDataStream();

        virtual void Write(const AppFrame::s32& data);
        virtual void Write(const AppFrame::u32& data);
        virtual void Write(const AppFrame::s16& data);
        virtual void Write(const AppFrame::u16& data);
        virtual void Write(const AppFrame::f32& data);

        virtual bool Read(AppFrame::s32& data);
        virtual bool Read(AppFrame::u32& data);
        virtual bool Read(AppFrame::s16& data);
        virtual bool Read(AppFrame::u16& data);
        virtual bool Read(AppFrame::f32& data);
    protected:
    private:
};

#endif // CNETDATASTREAM_H
