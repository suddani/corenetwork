/*
  Copyright (C) 2012 Daniel Sudmann

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

    1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.

  Daniel Sudmann suddani@googlemail.com
*/

#include <core_net/CStreamManager.h>
#include <core_net/CConnectionManager.h>
#include <core_net/CStreamEventManager.h>
#include <core_net/CActorStreamManager.h>

#include <core/ILog.h>
#include <core_net/CNetDataStream.h>

#include <time.h>

class CStreamManagerTimer : public CStreamManager::Timer
{
public:
    int getTime()
    {
        return clock();
    }

    int getTicksPerSecond()
    {
        return 1000;
    }
};

CStreamManager::CStreamManager(CConnectionManager* connection, CStreamManager::Timer* _timer)
{
    //ctor
    setUpdateRate(10, 500);
    last_update = 0;
    timer = _timer ? _timer : new CStreamManagerTimer();
    event_manager = new CStreamEventManager();
    actor_manager = new CActorStreamManager();
    connection_manager = connection;

    //set the actor event receiver
    event_manager->setActorMessageReceiver(actor_manager);

    //grabbing yourself so that the connection manager can't delete the scenemanager
    // TODO (Sudi#1#): HACK.....fix it
    this->grab();
    connection_manager->setMessageReceiver(this);
}

CStreamManager::~CStreamManager()
{
    //dtor
}

CStreamEventManager* CStreamManager::getEventManager(void)
{
    return event_manager;
}

CActorStreamManager* CStreamManager::getActorManager(void)
{
    return actor_manager;
}

void CStreamManager::setTimer(Timer* t)
{
    timer = t;
}

bool CStreamManager::update()
{
    if (last_update+getTime2Update() > timer->getTime())
        return false;

    packet_size_reliable = 0;
    packet_size_unreliable = 0;

    int event_reliable = 0;
    int event_unreliable = 0;
    //int actor_reliable = 0;
    //int actor_unreliable = 0;

    //for every connection assemble its packets
    for (int i=0;i<connection_manager->getConnectionCount();i++)
    {
        CConnection* connection = connection_manager->getConnection(i);
        if (!connection)
            nlerror<<"Accesing an illigal Connection"<<nlendl;

        bool data_to_send = false;


        //first dispatch reliable events!
        CNetDataStream out;
        data_to_send = event_manager->onUpdate(&out, true, connection) ? true : data_to_send;
        if (data_to_send)
            event_reliable += out.getSize();
        data_to_send = actor_manager->onUpdate(&out, true, connection) ? true : data_to_send;
        if (data_to_send)
        {
            connection_manager->send(connection->getId(), &out, CC_EVENT_RELIABLE);
            packet_size_reliable += out.getSize();
        }

        //now unreliable
        out.reset();
        data_to_send = event_manager->onUpdate(&out, false, connection) ? true : data_to_send;
        if (data_to_send)
            event_unreliable += out.getSize();
        data_to_send = actor_manager->onUpdate(&out, false, connection) ? true : data_to_send;
        if (data_to_send)
        {
            connection_manager->send(connection->getId(), &out, CC_EVENT_UNRELIABLE);
            packet_size_unreliable += out.getSize();
        }
    }

    last_update = timer->getTime();

    event_manager->tick();
    actor_manager->tick();

    //nlog<<"Reliable["<<packet_size_reliable<<"] Unreliable["<<packet_size_unreliable<<"]"<<nlendl;
    //nlog<<"EventReliable["<<event_reliable<<"] EventUnreliable["<<event_unreliable<<"]"<<nlendl;

    return true;
}

void CStreamManager::setUpdateRate(const int& pps, const int& ps)
{
    packets_per_second = pps;
    packet_size = ps;
}

int CStreamManager::getTime2Update(void)
{
    return timer->getTicksPerSecond()/packets_per_second;
}

void CStreamManager::setEventReceiver(IConnectionMessageReceiver* receiver)
{
    connection_receiver = receiver;
}

void CStreamManager::replicate(AppFrame::SharedPointer<AppFrame::actions::Message> msg, const int& actor_id, bool reliable, const ConnectionID& receiver, bool only)
{
    msg->UserID = actor_id;
    replicate(msg, reliable, receiver, only);
}

void CStreamManager::replicate(AppFrame::SharedPointer<AppFrame::actions::Message> msg, bool reliable, const ConnectionID& receiver, bool only)
{
    for (int i=0;i<connection_manager->getConnectionCount();i++)
    {
        CConnection* connection = connection_manager->getConnection(i);
        if (receiver == -1)
        {
            connection->getStreamEventCollection()->addEvent(msg, reliable);
        }
        else if (receiver == connection->getId() && only)
        {
            connection->getStreamEventCollection()->addEvent(msg, reliable);
            break;//exit bc there is nothing else todo
        }
        else if (receiver != connection->getId() && !only)
        {
            connection->getStreamEventCollection()->addEvent(msg, reliable);
        }
    }
}

void CStreamManager::onConnect(const ConnectionID& id)
{
    if (connection_receiver)
        connection_receiver->onConnect(id);
}

void CStreamManager::onDisconnect(const ConnectionID& id)
{
    if (connection_receiver)
        connection_receiver->onDisconnect(id);
}

void CStreamManager::onMessage(const ConnectionID& id, const CONNECTION_CHANNEL& channel, AppFrame::IDataStream* stream)
{
    if ((channel == CC_DATA_RELIABLE || channel == CC_DATA_UNRELIABLE) && connection_receiver)
        connection_receiver->onMessage(id, channel, stream);
    else if (channel == CC_EVENT_RELIABLE)
    {
        event_manager->onReceive(stream, id);
        actor_manager->onReceiveEvents(stream, connection_manager->getConnectionById(id));
    }
    else if (channel == CC_EVENT_UNRELIABLE)
    {
        event_manager->onReceive(stream, id);
        actor_manager->onReceive(stream, connection_manager->getConnectionById(id));
    }
}

