/*
  Copyright (C) 2012 Daniel Sudmann

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.

  Daniel Sudmann suddani@googlemail.com
*/

#include <core_net/CActorStreamManager.h>
#include <core_net/CActor.h>
#include <core_net/CStreamActorCollection.h>
#include <core_net/CConnection.h>
#include "CNetDataStream.h"
#include <core_net/IActorReceiver.h>

#include <core/IDataStream.h>

CActorStreamManager::CActorStreamManager()
{
    allowRemoteActorCreation = false;
    actor_receiver = NULL;
}

CActorStreamManager::~CActorStreamManager()
{
    //dtor
}

void CActorStreamManager::setActorReceiver(IActorReceiver* receiver)
{
    actor_receiver = receiver;
}


void CActorStreamManager::setAllowRemoteActorCreation(bool allow)
{
    allowRemoteActorCreation = allow;
}

bool CActorStreamManager::onUpdate(AppFrame::IDataStream* out, bool reliable, CConnection* connection)
{
    return (reliable ? onSendActorEvents(out, connection) : onSendActors(out, connection));
}

void CActorStreamManager::registerActorDynamic(CActor* actor)
{
    if (!actor->isTemp())
    {
        actor->setId(actor_ids.add(actor));
        addHandler(actor);
    }
    else
        temporary_actors.push_back(actor);
}

void CActorStreamManager::removeDynamicActor(CActor* actor)
{
    removeHandler(actor);
}

void CActorStreamManager::requestActorDynamic(CActor* actor)
{
    actor_ids.add(actor, actor->getId());
    addHandler(actor);
}

int CActorStreamManager::registerClass(const char* type)
{
    class_names.push_back(type);
    return class_names.size()-1;
}
//!Events
/*!
    int - Number of delete events
        int - actor id
    int - Number of create events
        int - actor id
        int - actor class_id
    int - Number of actors created on foreign host
        int - actor id
*/
bool CActorStreamManager::onReceiveEvents(AppFrame::IDataStream* in, CConnection* connection)
{
    bool events_pending = false;
    in->Read(events_pending);
    if (!events_pending)
        return true;

    int count = 0;
    in->Read(count);
    for (int i=0;i<count;i++)
    {
        int actor_id;
        in->Read(actor_id);
        if (allowRemoteActorCreation)
        {
            CActor* actor = actor_ids.getObject(actor_id);
            if (actor)
            {
                //!Delete Actor
                if (actor_receiver)
                    actor_receiver->onActorDestroy(actor);
                removeHandler(actor);
                nlog<<"Received destroy Actor command"<<nlendl;
            }
        }
    }
    in->Read(count);
    for (int i=0;i<count;i++)
    {
        int actor_id;
        int class_id;
        in->Read(actor_id);
        in->Read(class_id);
        if (allowRemoteActorCreation)
        {
            CActor* actor = actor_ids.getObject(actor_id);
            if (actor && actor->getClassId() == class_id)
            {
                nlog<<"Actor already exists server just doesn't know it yet..tell him again"<<nlendl;
                actors_created.push_back(actor_id);
            }
            else if (!actor)
            {
                //!Create Actor
                nlog<<"Received create Actor command["<<class_id<<", "<<actor_id<<"]"<<nlendl;
                CActor* actor = new CActor(class_id, actor_id, false);
                requestActorDynamic(actor);
                if (actor_receiver)
                    actor_receiver->onActorCreate(class_names[class_id].c_str(), actor);
                actors_created.push_back(actor_id);
            }
            else
            {
                nlerror<<"Terminate connection something went wrong"<<nlendl;
            }
        }
    }
    in->Read(count);
    for (int i=0;i<count;i++)
    {
        int actor_id;
        in->Read(actor_id);
        //nlog<<"Client created actor as asked. Now we can start sending updates: "<<actor_id<<nlendl;
        CActor* actor = actor_ids.getObject(actor_id);
        if (actor)
        {

            connection->getStreamActorCollection()->addActor(actor_ids.getObject(actor_id));
        }
    }
    return true;
}

bool CActorStreamManager::onSendActorEvents(AppFrame::IDataStream* out, CConnection* connection)
{
    //nlog<<"Send Actor Events..."<<nlendl;
    CStreamActorCollection* collection = connection->getStreamActorCollection();

    CStreamActorCollection::ActorStateArrayPtr client_actors = collection->getActors();

    std::vector<CActor*> to_create;
    std::vector<CActor*> to_delete;

    //figure out what actors need to be created
    RunnableListIt it = Runnables.begin();
    while (it != Runnables.end())
    {
        if (!collection->has(*it))
            to_create.push_back(*it);
        it++;
    }
    //figure out what actors need to be deleted
    for (unsigned int i=0;i<client_actors.size();i++)
    {
        if (actor_ids.getId(client_actors[i]->actor) == -1)
        {
            to_delete.push_back(client_actors[i]->actor);

            //Remove from the collection
            collection->removeActor(client_actors[i]->actor);
        }
    }

    if (to_delete.size() > 0 || to_create.size() > 0 || actors_created.size() > 0)
        out->Write((bool)true);
    else
    {
        out->Write((bool)false);
        return false;
    }
    ///Write actors to delete
    out->Write(to_delete.size());
    for (unsigned int i=0;i<to_delete.size();i++)
    {
        out->Write(to_delete[i]->getId());
    }
    ///Write actors to delete
    out->Write(to_create.size());
    for (unsigned int i=0;i<to_create.size();i++)
    {
        out->Write(to_create[i]->getId());
        out->Write(to_create[i]->getClassId());
    }
    ///Write which actors have been created by request
    out->Write(actors_created.size());
    for (unsigned int i=0;i<actors_created.size();i++)
    {
        out->Write(actors_created[i]);
    }
    actors_created.clear();
    //nlog<<"Send Actor Events...Done"<<nlendl;
    return true;
}

bool CActorStreamManager::onSendActors(AppFrame::IDataStream* out, CConnection* connection)
{
    //nlog<<"Send Actors..."<<nlendl;
    CStreamActorCollection* collection = connection->getStreamActorCollection();
    CStreamActorCollection::ActorStateArrayPtr actors = collection->getActors();
    out->Write(actors.size()+temporary_actors.size());
    for (unsigned int i=0;i<actors.size();i++)
    {
        CNetDataStream actor_stream;
        actors[i]->actor->serialize(&actor_stream, actors[i]);
        actors[i]->bNetInitial = false;
        out->Write(&actor_stream);
    }
    for (unsigned int i=0;i<temporary_actors.size();i++)
    {
        CNetDataStream actor_stream;
        temporary_actors[i]->serialize(&actor_stream, NULL);
        out->Write(&actor_stream);
    }
    //nlog<<"Send Actors...Done"<<nlendl;
    return true;
}

bool CActorStreamManager::onReceive(AppFrame::IDataStream* in, CConnection* connection)
{
    int actor_count = 0;
    in->Read(actor_count);
    //if (actor_count > 0)
    //    nlog<<"Received updates for "<<actor_count<<" Actor(s)"<<nlendl;
    for (int i=0;i<actor_count;i++)
    {
        CNetDataStream actor_stream_;
        AppFrame::IDataStream* actor_stream = &actor_stream_;
        in->Read(actor_stream);

        int id = -1;
        int class_id = -1;
        bool temporary = false;
        actor_stream->Read(id);
        actor_stream->Read(class_id);
        actor_stream->Read(temporary);
        if (!temporary)
        {
            CActor* actor = actor_ids.getObject(id);
            if (actor)
            {
                actor->deserialize(actor_stream, connection->getStreamActorCollection()->getActorState(actor->getId()));
            }
        }
        else
        {
            //!Create node, fire callback and let node die
            AppFrame::SharedPointer<CActor> actor = new CActor(class_id, true);
            if (actor_receiver)
                actor_receiver->onActorCreateTemporary(class_names[class_id].c_str(), actor);
            actor->deserialize(actor_stream, NULL);
        }
    }
    return true;
}

void CActorStreamManager::onAdd(AppFrame::SharedPointer<CActor> actor)
{

}

void CActorStreamManager::onRemove(AppFrame::SharedPointer<CActor> actor)
{
    actor_ids.remove(actor);
}

void CActorStreamManager::tick(void)
{
    temporary_actors.clear();
}

AppFrame::actions::MessageDispatcher* CActorStreamManager::getActorById(const int& id)
{
    return actor_ids.getObject(id);
}




