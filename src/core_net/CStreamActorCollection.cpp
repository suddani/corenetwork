/*
  Copyright (C) 2012 Daniel Sudmann

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.

  Daniel Sudmann suddani@googlemail.com
*/

#include <core_net/CStreamActorCollection.h>
#include <core_net/CActor.h>

CStreamActorCollection::CStreamActorCollection()
{
    //ctor
}

CStreamActorCollection::~CStreamActorCollection()
{
    //dtor
}

void CStreamActorCollection::addActor(CActor* actor)
{
    if (actor_states.find(actor->getId()) != actor_states.end())
        return;
    actor_states.insert(ActorStatePair(actor->getId(), CActorReplicationState(actor)));
}

bool CStreamActorCollection::has(CActor* actor)
{
    return actor_states.find(actor->getId()) != actor_states.end();
}

void CStreamActorCollection::removeActor(CActor* actor)
{
    actor_states.erase(actor->getId());
}

CActorReplicationState* CStreamActorCollection::getActorState(int id)
{
    ActorStateItr it = actor_states.find(id);
    if (it != actor_states.end())
        return &(*it).second;
    return NULL;
}

CStreamActorCollection::ActorStateArrayPtr CStreamActorCollection::getActors(void)
{
    ActorStateArrayPtr states;
    ActorStateItr it = actor_states.begin();
    while (it != actor_states.end())
    {
        states.push_back(&(*it).second);
        it++;
    }
    return states;
}

