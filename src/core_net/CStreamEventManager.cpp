/*
  Copyright (C) 2012 Daniel Sudmann

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.

  Daniel Sudmann suddani@googlemail.com
*/

#include <core_net/CStreamEventManager.h>
#include <core/IDataStream.h>
#include <core_net/CConnection.h>
#include <core_net/IActorEventMessenger.h>

CStreamEventManager::CStreamEventManager()
{
    next_event_id = 0;
    actor_events = NULL;
}

CStreamEventManager::~CStreamEventManager()
{
}

bool CStreamEventManager::onReceive(AppFrame::IDataStream* in, const ConnectionID& connection)
{
    int event_count = 0;
    in->Read(event_count);
    for (int i=0;i<event_count;i++)
    {
        int event_type = 0;
        int receiver_id = -1;
        in->Read(event_type);
        in->Read(receiver_id);
        AppFrame::actions::Message* msg = this->create(event_type);
        if (msg)
        {
            msg->deserialize(in);
            msg->UserID = connection; //sets the connectionId as user that way the origin of the event can be determined
            if (receiver_id != -1 && actor_events)
            {
                AppFrame::actions::MessageDispatcher* actor = actor_events->getActorById(receiver_id);
                if (actor)
                    actor->send(msg);
            }
            else
                this->send(msg);
            delete msg;
        }
        else
        {
            nlerror<<"Error while receiving events!"<<nlendl;
            return false;
        }
    }
    return true;
}

bool CStreamEventManager::onUpdate(AppFrame::IDataStream* out, bool reliable, CConnection* connection)
{
    int message_count = connection->getStreamEventCollection()->getSize(reliable);
    out->Write(message_count);
    if (message_count == 0)
        return false;

    AppFrame::actions::Message* msg = connection->getStreamEventCollection()->pop(reliable);
    while(msg)
    {
        if (message_map.find(msg->getID()) != message_map.end())
        {
            int creator_id = message_map[msg->getID()];
            out->Write(creator_id);
            out->Write(msg->UserID);
            msg->serialize(out);
        }
        else
        {
            // TODO (Sudi#1#): Check if the other host actually crashes and if yes fix it!
            nlerror<<"Tried sending a not registred message...the host will now crash!"<<nlendl;
        }
        msg->drop();
        msg = connection->getStreamEventCollection()->pop(reliable);
    }
    return true;
}

void CStreamEventManager::setActorMessageReceiver(IActorEventMessenger* receiver)
{
    actor_events = receiver;
}


