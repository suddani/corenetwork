/*
  Copyright (C) 2012 Daniel Sudmann

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.

  Daniel Sudmann suddani@googlemail.com
*/

#include <core_net/CStreamEventCollection.h>

#include <core/MessageDispatcher.h>
#include <core/ILog.h>

CStreamEventCollection::CStreamEventCollection()
{
    //ctor
}

CStreamEventCollection::~CStreamEventCollection()
{
    while(out_reliable.size()>0)
    {
        AppFrame::actions::Message* msg = out_reliable[0];
        msg->drop();
        out_reliable.erase(out_reliable.begin());
    }
    while(out_unreliable.size()>0)
    {
        AppFrame::actions::Message* msg = out_unreliable[0];
        msg->drop();
        out_unreliable.erase(out_unreliable.begin());
    }
}

void CStreamEventCollection::addEvent(AppFrame::actions::Message* msg, bool reliable)
{
    //nlog<<"Add message for sending"<<nlendl;
    msg->grab();
    if (reliable)
        out_reliable.push_back(msg);
    else
        out_unreliable.push_back(msg);
    //nlog<<"There are "<<out_reliable.size()<<" reliable messages"<<nlendl;
    //nlog<<"There are "<<out_unreliable.size()<<" unreliable messages"<<nlendl;
}

int CStreamEventCollection::getSize(bool reliable)
{
    if (reliable)
    {
        //nlog<<"Should return "<<out_reliable.size()<<nlendl;
        return out_reliable.size();
    }
    else
    {
        //nlog<<"Should return "<<out_unreliable.size()<<nlendl;
        return out_unreliable.size();
    }
}


AppFrame::actions::Message* CStreamEventCollection::pop(bool reliable)
{
    AppFrame::actions::Message* msg = NULL;
    if (reliable && out_reliable.size() > 0)
    {
        msg = out_reliable[0];
        out_reliable.erase(out_reliable.begin());
    }
    else if(!reliable && out_unreliable.size() > 0)
    {
        msg = out_unreliable[0];
        out_unreliable.erase(out_unreliable.begin());
    }
    return msg;
}


