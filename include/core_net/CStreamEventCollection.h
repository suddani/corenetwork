#ifndef CSTREAMEVENTCOLLECTION_H
#define CSTREAMEVENTCOLLECTION_H

#include <vector>

namespace AppFrame
{
    namespace actions
    {
        class Message;
    }
}
class CStreamEventCollection
{
    public:
        CStreamEventCollection();
        virtual ~CStreamEventCollection();

        /** \brief Adds a Message to this event collection.
         *
         * \param msg The message that is added. This function calls grab on the message
         * \param reliable This determins if the message is reliable or not.
         *
         */
        void addEvent(AppFrame::actions::Message* msg, bool reliable);

        /** \brief This function pops the first message from the message stack
         *
         * \param reliable Determens if you pop a message from the reliable or unreliable stack
         * \return The message if there is one or NULL. When you are done with the message object you should call drop on it.
         *
         */
        AppFrame::actions::Message* pop(bool reliable);

        /** \brief Gets the size of the message stacks
         *
         * \param reliable If true the size of the reliable stack is returned. If false the unreliable stack size is returned.
         * \return The stacksize. Keep in mind this value changes with every addEvent and pop
         *
         */
        int getSize(bool reliable);
    protected:
        std::vector<AppFrame::actions::Message*> out_unreliable;
        std::vector<AppFrame::actions::Message*> out_reliable;
    private:
};

#endif // CSTREAMEVENTCOLLECTION_H
