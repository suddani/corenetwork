#ifndef IACTOREVENTMESSENGER_H
#define IACTOREVENTMESSENGER_H

namespace AppFrame
{
    namespace actions
    {
        class MessageDispatcher;
    }
}
class IActorEventMessenger
{
    public:

        /** \brief This function shall return a MessageDispatcher that belongs to a specific actor id
         *
         * \param id The actor id.
         * \return If there is a dispatcher for that actor it is returned. otherwise NULL.
         */
        virtual AppFrame::actions::MessageDispatcher* getActorById(const int& id) = 0;
    protected:
    private:
};

#endif // IACTOREVENTMESSENGER_H
