#ifndef CSTREAMACTORCOLLECTION_H
#define CSTREAMACTORCOLLECTION_H

#include <map>
#include <vector>

#include <core/Tools.h>
#include <core_net/CActorReplicationState.h>

class CActor;
class CStreamActorCollection
{
    public:
        typedef std::map<int, CActorReplicationState > ActorStateMap;
        typedef std::pair<int, CActorReplicationState > ActorStatePair;
        typedef std::map<int, CActorReplicationState >::iterator ActorStateItr;
        typedef std::vector<CActorReplicationState*> ActorStateArrayPtr;

        CStreamActorCollection();
        virtual ~CStreamActorCollection();

        /** \brief Add an actor to a actor collection
         *
         * \param actor The actor that is added to the stream. Incase the same actor is already in the stream nothing happens
         *
         */
        void addActor(CActor* actor);

        /** \brief Removes an actor from the collection
         *
         * \param actor The actor that is removed to the stream. Incase the actor is not in the stream nothing happens
         *
         */
        void removeActor(CActor* actor);

        /** \brief Checks wether or not an actor is in the collection
         *
         * \return True if the actor is in the collection. Otherwise False
         *
         */
        bool has(CActor* actor);

        CActorReplicationState* getActorState(int id);

        ActorStateArrayPtr getActors(void);
    protected:
        ActorStateMap actor_states;
    private:
};

#endif // CSTREAMACTORCOLLECTION_H
