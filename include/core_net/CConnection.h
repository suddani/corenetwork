#ifndef CCONNECTION_H
#define CCONNECTION_H

#include <core/IRef.h>
#include <core/Tools.h>
#include <enet/enet.h>
#include <core_net/core_net.h>
#include <core_net/CStreamEventCollection.h>
#include <core_net/CStreamActorCollection.h>

class CConnection : public AppFrame::IRef
{
    public:
        CConnection(ENetPeer* peer_);
        virtual ~CConnection();

        /** \brief Returns the connection id.
         *
         * \return The id of this connection.
         *
         */
        const ConnectionID& getId() const;

        /** \brief Sets the id of this connection. (Should not be called. Internal use only!!)
         *
         * \param new_id The id of this connection.
         *
         */
        void setId(const ConnectionID& new_id);


        /** \brief Returns the StreamEventCollection
         *
         * \return The StreamEventCollection
         *
         */
        CStreamEventCollection* getStreamEventCollection(void);

        /** \brief Returns the StreamActorCollection
         *
         * \return The StreamActorCollection
         *
         */
        CStreamActorCollection* getStreamActorCollection(void);

        bool operator==(ENetPeer* other_peer);


        /** \brief Returns the enet_peer of this connection
         *
         * \return The enet_peer.
         *
         */
        ENetPeer* getPeer(void);
    protected:
        ENetPeer* peer;
        ConnectionID id;

        CStreamEventCollection events;
        CStreamActorCollection actors;
    private:
        static ConnectionID current_id;
};

#endif // CCONNECTION_H
