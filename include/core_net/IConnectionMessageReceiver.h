#ifndef ICONNECTIONMESSAGERECEIVER_H
#define ICONNECTIONMESSAGERECEIVER_H

#include <core/IRef.h>
#include <core/Tools.h>
#include <core/IDataStream.h>

#include <core_net/core_net.h>

class IConnectionMessageReceiver : public AppFrame::IRef
{
    public:
        /** \brief Notifies of a new connection
         *
         * \param id The id of the new connection. Can be used to send messages to a specific host with CConnectionManager
         */
        virtual void onConnect(const ConnectionID& id) = 0;

        /** \brief Notifies of a closed connection
         *
         * \param id The id of the closed connection.
         */
        virtual void onDisconnect(const ConnectionID& id) = 0;

        /** \brief Notifies of new data received
         *
         * \param id The id of the connection that send the data. Can be used to send messages to a specific host with CConnectionManager
         * \param channel The channel the data was send on
         * \param stream The actual data. This pointer is not valid after the function exits.
         */
        virtual void onMessage(const ConnectionID& id, const CONNECTION_CHANNEL& channel, AppFrame::IDataStream* stream) = 0;
    protected:
    private:
};

#endif // ICONNECTIONMESSAGERECEIVER_H
