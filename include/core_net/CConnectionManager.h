#ifndef CCONNECTIONMANAGER_H
#define CCONNECTIONMANAGER_H

#include <core/IRef.h>
#include <core/Tools.h>

#include <enet/enet.h>

#include <core_net/core_net.h>
#include <core_net/CConnection.h>

namespace AppFrame
{
    class IDataStream;
}
class IConnectionMessageReceiver;

class CConnectionManager : public AppFrame::IRef
{
    public:
        CConnectionManager();
        virtual ~CConnectionManager();
        // TODO (Sudi#1#): Needs a disconnect option....lol

        /** \brief Initialize the Connection Manager. If initialized with port 0 you can't connect to this host(client)
         *
         * \param port The port to bind to.
         * \param max_connections The maximum connection count. In and out going.
         */
        bool init(const short& port = 0, const int& max_connections = 1);


        /** \brief Connects this StreamManager to another Host
         *
         * \param address The host address. Either an ip or DNS name.
         * \param port The the port to connect to.
         */
        bool connect(const char* address, const short& port);

        /** \brief sends stream to all connected managers over the specified channel
         *
         * \param stream The payload to be send
         * \param channel The channel to use
         *
         * Should not be called for every peace of data to be transmitted bc it creates an extra packet.
         * Try to assemble data into bigger packets.
         */
        void send(AppFrame::IDataStream* stream, const CONNECTION_CHANNEL& channel);


        /** \brief sends stream to specified connection over the specified channel
         *
         * \param connection The connection to send the stream to
         * \param stream The payload to be send
         * \param channel The channel to use
         *
         * Should not be called for every peace of data to be transmitted bc it creates an extra packet.
         * Try to assemble data into bigger packets.
         */
        void send(const ConnectionID& connection, AppFrame::IDataStream* stream, const CONNECTION_CHANNEL& channel);

        /** \brief ticks the connection manager. Receives and send packets
        */
        void update();

        /** \brief sends stream to specified connection over the specified channel
         *
         * \param receiver The network receiver which is used to dispatch the network events to.
         * keep in mind that this pointer is automaticly dropped so either pass in a pointer which can be destroyed or grab the pointer before hand
         */
        void setMessageReceiver(IConnectionMessageReceiver* receiver);

        /** \brief Returns the ammount of connection
         *
         * \return The ammount of currently connected hosts
         *
         */
         int getConnectionCount(void);

        /** \brief Returns an connection by position
         *
         * \param The position of an Connection. This is not a connectionId
         * \return The connection if there is one at that position or NULL.
         *
         */
        CConnection* getConnection(const int& id);

        /** \brief Returns an connection by its id
         *
         * \param This is a connectionId assosiated with a connection
         * \return The connection if there is one with that id or NULL.
         *
         */
        CConnection* getConnectionById(const ConnectionID& id);

        /** \brief Returns all connections currently running
         *
         * \return An array filled with all connections. Don't delete them!
         *
         */
        std::vector<AppFrame::SharedPointer<CConnection> > getConnections(void);
    protected:
        ENetHost* host;

        AppFrame::SharedPointer<IConnectionMessageReceiver> messageReceiver;

        std::vector<AppFrame::SharedPointer<CConnection> > connections;

        CConnection* getConnectionByPeer(ENetPeer* peer);
    private:
        static bool initialized;
        static void initialize();
};

#endif // CCONNECTIONMANAGER_H
