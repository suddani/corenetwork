#ifndef CACTOR_H
#define CACTOR_H

#include <core/IRef.h>
#include <core/Tools.h>
#include <core/MessageDispatcher.h>

#include <core_net/core_net.h>

//attached by the class that uses the actor...i think
class IActorReplicationStatement
{
public:
protected:
private:
};

namespace AppFrame
{
    class IDataStream;
}
class CActorReplicationState;
class CActor : public AppFrame::IRef, public AppFrame::actions::MessageDispatcher
{
public:
    CActor(const int& class_id_, bool tmp);
    CActor(const int& class_id_, const int& actor_id, bool tmp);
    virtual ~CActor(void);

    /** \brief Serializes this actor to a IDataStream
     *
     * This function writes the id, class_id and bNetTemporary to the stream before writing any Actor specific data.\n
     *
     * \param out The stream to write to.
     * \param replication_state The replication state of this Actor for a specific connection
     *
     */
    void serialize(AppFrame::IDataStream* out, CActorReplicationState* replication_state);

    /** \brief Deserializes this actor from a IDataStream
     *
     * This function doesn't read the id, class_id and bNetTemporary from the stream. Those fields have to be read before calling deserialize\n
     *
     * \param in The stream to read from.
     * \param replication_state The replication state of this Actor for a specific connection
     *
     *
     */
    void deserialize(AppFrame::IDataStream* in, CActorReplicationState* replication_state);

    /** \brief Returns the id of this Actor
     *
     * This id is unique across all hosts. Except if the bNetTemporary flag is true. Then the id is random.
     *
     * \return The id.
     *
     */
    int getId(void);

    /** \brief Sets the id of this Actor. Don't use!!! Internal use only!!!
     *
     * \param id_ The new id.
     *
     */
    void setId(int id_);

    /** \brief Returns the class_id of this Actor
     *
     * \return The class_id.
     *
     */
    int getClassId(void);

    /** \brief Returns wether or not this actor is temporary and therefor only replicated once unreliably.
     *
     * \return If True this actory is temporary.
     *
     */
    bool isTemp(void);
protected:
    //Relevanz
	bool bAlwaysRelevant;
	bool bOnlyRelevantToOwner;
	//(Owner == Player) player is the currently being replicated player
	bool bHidden;
	bool bBlockPlayers;
	//(bHidden = true) and it doesn't collide (bBlockPlayers = false) and it doesn't have an ambient sound (AmbientSound == None)
	//line of sight check

	float NetPriority;
	int NetReplicationTime;

	//or make extra event callbacks for init and deletion to automate it
	//called on the server when a new proxy is created on a client maschine
	void EventInit(const ConnectionID& id);
	//Called on proxy nodes when the authority is deleted
	void EventRemoved(void);

	void init(const int& class_id_, bool tmp);

	//data to write
	int id;
	int class_id;

	//bool bStatic; //instanciated and left on the maschine
	//bool bNoDelete; //don't delete even if the server says so

	bool bNetTemporary; //only replicated once
	bool bTearOff; //if this turns to true the actor will gain full control and there won't be any update packets anymore
private:
};

#endif // CACTOR_H
