#ifndef CACTORREPLICATIONSTATE_H
#define CACTORREPLICATIONSTATE_H

#include <core/Tools.h>

class CActor;
class CActorReplicationState
{
public:
    CActorReplicationState(void);
    CActorReplicationState(CActor* actor);
    virtual ~CActorReplicationState(void);

	bool bNetDirty;
	bool bNetInitial;
	bool bNetOwner;
	AppFrame::SharedPointer<CActor> actor;
protected:
    void init(void);
};

#endif // CACTORREPLICATIONSTATE_H
