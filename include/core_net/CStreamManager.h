#ifndef CSTREAMMANAGER_H
#define CSTREAMMANAGER_H

#include <core/IRef.h>
#include <core/Tools.h>
#include <core/CRunnableList.h>

#include <core_net/core_net.h>
#include <core_net/IConnectionMessageReceiver.h>

class CStreamEventManager;
class CConnectionManager;
class CActorStreamManager;
namespace AppFrame
{
    namespace actions
    {
        class Message;
    }
}

/** \brief This classes manages all streams
 * On linux or any other os than windows you will probably have to use your own CStreamManager::Timer
 *
 */
class CStreamManager : public IConnectionMessageReceiver
{
public:
    /** \brief This class is used to time the StreamManager update rate
     */
    class Timer : public AppFrame::IRef
    {
    public:
        /** \brief This function should return the current time
         *
         * \return The current time
         *
         */
        virtual int getTime(){return 0;}

        /** \brief This function should return the ammount of ticks in a second.(resolution)
         *
         * \return The resolution of the time
         *
         */
        virtual int getTicksPerSecond(){return 0;}
    };

    /** \brief Creates a Stream Manager
     *
     * \param connection This is the connection this StreamManager will use
     * \param _timer The timer this StreamManager should use. If NULL it uses the standard timer.
     */
    CStreamManager(CConnectionManager* connection, CStreamManager::Timer* _timer = NULL);
    virtual ~CStreamManager();

    /** \brief Updates the StreamManager
     * \return returns true if the manager just send out a packet
     */
    bool update();

    /** \brief Sets the timer the Streammanager should use.
     * \param t The timer this StreamManager should use. Never pass in NULL
     */
    void setTimer(Timer* t);

    /** \brief Sets the update rate and max packet size the StreamManager should use.
     * \param pps The ammount of packets to send per second
     * \param ps The maximal packet size
     */
    void setUpdateRate(const int& pps, const int& ps);

    /** \brief Get the time between updates in ticks of the timer resolution
     * \return time between updates
     */
    int getTime2Update(void);

    /** \brief The Function returns the EventStreamManager to work with network events
     *
     * \return The EventStreamManager
     *
     */
    CStreamEventManager* getEventManager(void);


    /** \brief The Function returns the ActorStreamManager to work with network actors
     *
     * \return The ActorStreamManager
     *
     */
    CActorStreamManager* getActorManager(void);

    /** \brief Normaly the events would be swalloed by the stream manager but all that aren't are dispatched to a receiver if set
     *
     * \param receiver The receiver to dispatch network events to. Please beware that the receiver might be dropped so grab it if you want to keep it.
     */
    void setEventReceiver(IConnectionMessageReceiver* receiver);

    /** \brief Replicates an event message to other hosts
     *
     * \param msg The Message to be replicated. Inside the message is grabbed and dropped so incase you passed in a (new Message()) you are done.
     * \param reliable This determense if the message is supposed to be delivered reliable or not.
     * \param receiver The connection that is supposed to receive the Message. If this is set to -1 the message is dispatched to all connected hosts
     * \param only If receiver is a valid ConnectionID and only is true then the message is dispatched to that connection only. if Only is false then the message is dispatched to every connection but the specified in receiver
     *
     */
    void replicate(AppFrame::SharedPointer<AppFrame::actions::Message> msg, bool reliable, const ConnectionID& receiver = -1, bool only = true);

    /** \brief Replicates an event message to actors on other hosts
     *
     * \param msg The Message to be replicated. Inside the message is grabbed and dropped so incase you passed in a (new Message()) you are done.
     * \param actor_id The Actor this message is dispatched to incase he exists
     * \param reliable This determense if the message is supposed to be delivered reliable or not.
     * \param receiver The connection that is supposed to receive the Message. If this is set to -1 the message is dispatched to all connected hosts
     * \param only If receiver is a valid ConnectionID and only is true then the message is dispatched to that connection only. if Only is false then the message is dispatched to every connection but the specified in receiver
     *
     */
    void replicate(AppFrame::SharedPointer<AppFrame::actions::Message> msg, const int& actor_id, bool reliable, const ConnectionID& receiver = -1, bool only = true);

    //! To talk to the network. Inherited from IConnectionMessageReceiver and dispatches events to its own receiver
    void onConnect(const ConnectionID& id);
    //! To talk to the network. Inherited from IConnectionMessageReceiver and dispatches events to its own receiver
    void onDisconnect(const ConnectionID& id);
    //! To talk to the network. Inherited from IConnectionMessageReceiver and dispatches events to its own receiver incase they are sent on CC_DATA_RELIABLE or CC_DATA_UNRELIABLE
    void onMessage(const ConnectionID& id, const CONNECTION_CHANNEL& channel, AppFrame::IDataStream* stream);
protected:
    int packets_per_second;
    int packet_size;
    int last_update;

    AppFrame::SharedPointer<Timer> timer;

    AppFrame::SharedPointer<IConnectionMessageReceiver> connection_receiver;
    AppFrame::SharedPointer<CStreamEventManager> event_manager;
    AppFrame::SharedPointer<CActorStreamManager> actor_manager;

    // TODO (Sudi#1#): Probably a circular reference...has to be fixed!
    AppFrame::SharedPointer<CConnectionManager> connection_manager;

    int packet_size_reliable;
    int packet_size_unreliable;
private:
};

#endif // CSTREAMMANAGER_H
