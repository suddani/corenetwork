#ifndef NETWORKTYPES_H_INCLUDED
#define NETWORKTYPES_H_INCLUDED

/*! \page core_net Core_Net
 *
 * \section core_net_intro Introduction
 *
 * Core_net is a network layer for TCore
 *
 *
 * \section core_net_usage Usage
 * First of all you will need a connection. So create an instance of CConnectionManager.\n
 * To make the basic stuff run you need to include the following headers
 * \code
//This class is used to serialize stuff to the network. (It takes care of network byteorder)
#include <core/IDataStream.h>

//Those are the classes that make the magic actually work
#include <core_net/CConnectionManager.h>
#include <core_net/CStreamManager.h>
#include <core_net/CStreamEventManager.h>
#include <core_net/CActorStreamManager.h>
#include <core_net/CActor.h>

//Those two classes will give you callbacks into the inner workings
#include <core_net/IActorReceiver.h>
#include <core_net/IConnectionMessageReceiver.h>
 * \endcode
 *
 * \section core_net_examples Examples
 * \code
struct test_message : public AppFrame::actions::Message_<test_message>
{
    irr::core::stringc data;
    test_message()
    {
    }
    test_message(const char* msg)
    {
        data = msg;
    }
    ~test_message(void)
    {
    }
    virtual void serialize(AppFrame::IDataStream* stream)
    {
        stream->Write(data);
    }

    virtual void deserialize(AppFrame::IDataStream* stream)
    {
        stream->Read(data);
    }

    void print()
    {
        nlog<<"test_message received with "<<data.c_str()<<nlendl;
    }
};


struct test_message2 : public AppFrame::actions::Message_<test_message2>
{
    irr::core::stringc data;
    int health;
    test_message2()
    {
        health = 0;
    }
    test_message2(const char* msg, int h)
    {
        data = msg;
        health = h;
    }
    ~test_message2(void)
    {
    }
    virtual void serialize(AppFrame::IDataStream* stream)
    {
        stream->Write(data);
        stream->Write(health);
    }

    virtual void deserialize(AppFrame::IDataStream* stream)
    {
        stream->Read(data);
        stream->Read(health);
    }

    void print()
    {
        nlog<<"test_message2 received with "<<data.c_str()<<" and "<<health<<nlendl;
    }
};

class testreceiver : public IConnectionMessageReceiver
{
public:
    testreceiver(CConnectionManager* _m, CStreamManager* stream_manager_)
    {
        m = _m;
        stream_manager = stream_manager_;
    }
    void onConnect(const ConnectionID& id)
    {
        nlog<<"Player connected"<<nlendl;
    }
    void onDisconnect(const ConnectionID& id)
    {
        nlog<<"Player disconnected"<<nlendl;
    }
    void onMessage(const ConnectionID& id, const CONNECTION_CHANNEL& channel, AppFrame::IDataStream* stream)
    {
    }
    CConnectionManager* m;
    CStreamManager* stream_manager;
};

void onMessage(test_message* test)
{
    test->print();
}

int last = 0;
void onMessage2(test_message2* test)
{
    if (test->health > last+1)
        nlerror<<"Skipped "<<test->health-last<<" message(s)"<<nlendl;
    last = test->health;
    test->print();
}
class ActorReceiver : public IActorReceiver
{
public:
    void onActorCreate(const char* type, CActor* actor)
    {
        nlog<<"Please create Actor of type "<<type<<nlendl;
        actor->Bind(onMessage);
        actor->Bind(onMessage2);
    }

    void onActorCreateTemporary(const char* type, CActor* actor)
    {
        nlog<<"Please create temporary Actor of type "<<type<<nlendl;
    }

    void onActorDestroy(CActor* actor)
    {
        nlog<<"Please destroy Actor "<<actor->getId()<<nlendl;
    }
};

int main(int argc, char* argv[])
{
    CConnectionManager connection;
    CStreamManager stream_manager(&connection);
    stream_manager.setEventReceiver(new testreceiver(&connection, &stream_manager));
    stream_manager.setUpdateRate(60, 1);

    stream_manager.getEventManager()->registerMessage<test_message>();
    stream_manager.getEventManager()->registerMessage<test_message2>();

    int player_class = stream_manager.getActorManager()->registerClass("PLAYER");
    int bomb_class = stream_manager.getActorManager()->registerClass("BOMB");

    bool server = true;

    AppFrame::SharedPointer<CActor> actor;

    if (argc > 1)
    {
        server = false;
        nlog<<"Start Client"<<nlendl;
        connection.init(0, 1);
        connection.connect("127.0.0.1", 50000) ? nlog<<"Connecting..."<<nlendl : nlerror<<"Connection failed!"<<nlendl;

        stream_manager.replicate(new test_message("Hello Events"), true);
        stream_manager.replicate(new test_message2("Hello Events2", 567), true);

        stream_manager.getActorManager()->setAllowRemoteActorCreation(true);

        stream_manager.getActorManager()->setActorReceiver(new ActorReceiver());
    }
    else
    {
        nlog<<"Start Server"<<nlendl;
        connection.init(50000, 10);

        actor = new CActor(player_class, false);
        stream_manager.getActorManager()->registerActorDynamic(actor);
        for (int i=0;i<20;i++)
            stream_manager.getActorManager()->registerActorDynamic(new CActor(player_class, false));
    }

    int id = 0;
    while(true)
    {
        if (stream_manager.update() && server)
        {
            if (id > 100 && actor)
            {

                stream_manager.getActorManager()->removeDynamicActor(actor);
                actor = new CActor(player_class, false);
                stream_manager.getActorManager()->registerActorDynamic(actor);
                id = 0;
                stream_manager.getActorManager()->registerActorDynamic(new CActor(bomb_class, true));
                test_message2* t = new test_message2("jojojo PLAYER", id);
                t->UserID = actor->getId();
                stream_manager.replicate(t, true);
            }
            id++;
        }
        connection.update();
    }
    return 0;
}
 * \endcode
 */

#include <core/Types.h>
typedef AppFrame::s32 ConnectionID;

enum CONNECTION_CHANNEL
{
    CC_DATA_RELIABLE = 0,
    CC_DATA_UNRELIABLE,

    CC_EVENT_RELIABLE,
    CC_EVENT_UNRELIABLE,

    CC_ACTOR_UNRELIABLE,

    CC_ACTOR_EVENT_RELIABLE,
    CC_ACTOR_EVENT_UNRELIABLE,

    CC_COUNT
};

class CActor;
#include <core/CIDGenerator.h>
typedef AppFrame::CIDGenerator<CActor> ActorIDGenerator;
typedef AppFrame::CID<CActor> ActorID;

enum E_ACTOR_ROLE
{
    EAR_None = 0,           // No role at all.
    EAR_SimulatedProxy,     // Locally simulated proxy of this actor.
    EAR_AutonomousProxy,    // Locally autonomous proxy of this actor.
    EAR_Authority           // Authoritative control over the actor.
};

#endif // NETWORKTYPES_H_INCLUDED
