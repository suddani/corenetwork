#ifndef IACTORRECEIVER_H
#define IACTORRECEIVER_H

#include <core/IRef.h>
#include <core/Tools.h>
class CActor;
class IActorReceiver : public AppFrame::IRef
{
    public:
        IActorReceiver() {}
        virtual ~IActorReceiver() {}

        /** \brief Called when an actor is created.
         *
         * \param type The type of the actor
         * \param actor The Actor itself
         */
        virtual void onActorCreate(const char* type, CActor* actor) = 0;

        /** \brief Called when a temporary actor is created.
         *
         * Right after this function is the actors deserialize function called. After that the Actor will be dropped. So incase you want to keep the actor around for whatever reason you should grab him.
         *
         * \param type The type of the actor
         * \param actor The Actor itself.
         */
        virtual void onActorCreateTemporary(const char* type, CActor* actor) = 0;

        /** \brief Called when an actor is destroyed.
         *
         * \param actor The Actor itself before destruction.
         */
        virtual void onActorDestroy(CActor* actor) = 0;
    protected:
    private:
};

#endif // IACTORRECEIVER_H
