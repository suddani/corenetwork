#ifndef CACTORSTREAMMANAGER_H
#define CACTORSTREAMMANAGER_H

#include <core/IRef.h>
#include <core/Tools.h>
#include <core/CRunnableList.h>
#include <core_net/IActorEventMessenger.h>
#include <core_net/core_net.h>

#include <vector>

namespace AppFrame
{
    class IDataStream;
}
class CConnection;
class CActor;
class IActorReceiver;
class CActorStreamManager : public AppFrame::IRef, public IActorEventMessenger, AppFrame::CRunnableList<AppFrame::SharedPointer<CActor> >
{
    public:
        CActorStreamManager();
        virtual ~CActorStreamManager();

        /** \brief This function updates the ActorStream
         *
         * \param in The input DataStream containing the actors
         * \param connection The connection theses actors came from
         * \return Returns true if there was no error while receving the actors otherwise false
         */
        bool onReceive(AppFrame::IDataStream* in, CConnection* connection);

        /** \brief This function receives actor create and delete messages
         *
         * \param in The input DataStream containing the create/delete messages
         * \param connection The connection theses messages came from
         * \return Returns true if there was no error while receving the messages otherwise false
         */
        bool onReceiveEvents(AppFrame::IDataStream* in, CConnection* connection);

        /** \brief This function updates the ActorStream and serializes actors to the stream
         *
         * \param out The output DataStream to write the actors to
         * \param connection The connection to work on
         */
        bool onUpdate(AppFrame::IDataStream* out, bool reliable, CConnection* connection);

        /** \brief Register an dynamic actor to be replicated to other hosts. This host gets the authority role
         *
         * \param actor The actor to be replicated
         */
        void registerActorDynamic(CActor* actor);

        /** \brief Register an dynamic actor to receive updates.
         *
         * This function is called when a not yet existend dynamic or tag actor has been replicated to this host
         * \param actor The actor to be replicated
         *
         */
        void requestActorDynamic(CActor* actor);

        /** \brief This function registers a string as actor class
         *
         * With this function you can register new types as actor classes. These types will be given on the client host\n
         * when new actors are supposed to be created
         *
         * \param type The type to register
         * \return Returns the id of the new class
         */
        int registerClass(const char* type);

        /** \brief This removes a dynamic actor from the update stream
         *
         * \param actor The actor to be removed from the Stream.
         *
         */
        void removeDynamicActor(CActor* actor);

        /** \brief This sets the actor callback receiver
         *
         * The receiver is informed about actor creation and deleteion.
         * \param receiver The callback receiver.
         *
         */
        void setActorReceiver(IActorReceiver* receiver);

        /** \brief This AllowRemoteActorCreation flag
         *
         * \param allow If set to true connected hosts can create Actors on this system and push actor data.
         *
         */
        void setAllowRemoteActorCreation(bool allow);

        /** \brief Returns the MessageDispatcher that belongs to a specific Actor
         *
         * \param id The id of the Actor
         * \return The MessageDispatcher incase there is one for the given id.
         *
         */
        AppFrame::actions::MessageDispatcher* getActorById(const int& id);

        /** \brief This functions ticks the StreamManager to the next index
         *
         *
         */
        void tick(void);
    protected:
        std::vector<std::string> class_names;

        std::vector<int> actors_created;
        ActorIDGenerator actor_ids;
        std::vector<AppFrame::SharedPointer<CActor> > temporary_actors;

        IActorReceiver* actor_receiver;

        bool allowRemoteActorCreation;

        void onAdd(AppFrame::SharedPointer<CActor> actor);
        void onRemove(AppFrame::SharedPointer<CActor> actor);

        bool onSendActorEvents(AppFrame::IDataStream* out, CConnection* connection);
        bool onSendActors(AppFrame::IDataStream* out, CConnection* connection);
    private:
};

#endif // CACTORSTREAMMANAGER_H
