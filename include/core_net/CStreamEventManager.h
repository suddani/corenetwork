#ifndef CSTREAMEVENTMANAGER_H
#define CSTREAMEVENTMANAGER_H

#include <core/IRef.h>
#include <core/Tools.h>
#include <core/CGenericFactory.h>
#include <core/MessageDispatcher.h>
#include <core_net/core_net.h>

class CConnection;
class IActorEventMessenger;

/** \brief The NetworkEventManager
 *
 * This class is responsible for dispatching messages over the network to different hosts.
 * Messages received are destroyed after they have been dispatched so don't use them after receiving or sending them!!!
 */
class CStreamEventManager : public AppFrame::CGenericFactory<AppFrame::actions::Message*, int>, public AppFrame::actions::MessageDispatcher, public AppFrame::IRef
{
    public:
        CStreamEventManager();
        virtual ~CStreamEventManager();

        /** \brief Register a MessageType to be replicated over the EventManager
         * Message Types have to resgitered in the same order on all maschines
         */
        template<class MessageType>
        void registerMessage(void)
        {
            int id = next_event_id++;
            message_map.insert ( std::pair<int,int>(MessageType::getID_static(),id) );
            this->registerTypePointer<MessageType>(id);
        }

        /** \brief This function updates the EventStream and dispatches received events
         *
         * \param in The input DataStream containing the events
         * \param connection The connection theses events came from
         * \return Returns true if there was no error while receving the events otherwise false
         */
        bool onReceive(AppFrame::IDataStream* in, const ConnectionID& connection);

        /** \brief This function updates the EventStream and serializes events to the stream
         *
         * \param out The output DataStream to write the events to
         * \param reliable If this parameter is true only reliable messages are serialized. Otherwise the unreliable
         * \param connection The connection to work on
         */
        bool onUpdate(AppFrame::IDataStream* out, bool reliable, CConnection* connection);

        /** \brief This function sets the ActorMessage Dispatcher
         *
         * \param receiver The event receiver is used to dispatch the events to the correct actor
         *
         */
        void setActorMessageReceiver(IActorEventMessenger* receiver);

        /** \brief This functions ticks the StreamManager to the next index
         *
         *
         */
        void tick(void){}
    protected:

    private:
        /**< Do not change!!! Used for event mapping */
        int next_event_id;
        std::map<int, int> message_map;
        IActorEventMessenger* actor_events;
};

#endif // CSTREAMEVENTMANAGER_H
